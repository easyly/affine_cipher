# AFFINE CIPHER
The Affine Cipher is another example of a Monoalphabetic Substituiton cipher. Since the encryption process is substantially mathematical the whole process relies on working modulo m (the length of the alphabet used). By performing a calculation on the plaintext letters, we encipher the plaintext.


                                   E(x) = (ax + b) mod m


![img](http://crypto.interactive-maths.com/uploads/1/1/3/4/11345755/9236609_orig.jpg)
![img2](http://crypto.interactive-maths.com/uploads/1/1/3/4/11345755/9204085_orig.jpg)

## Weaknesses

- As each letter is encrypted using the same function, trying many possibilities for a or b can be useful to decrypt the message. This is called the **bruteforce method**.

- We can also use **frequency analysis** if the message is long to decrypt the message as the most common letters in english are :

<p align="center"> 
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/English_letter_frequency_%28alphabetic%29.svg/340px-English_letter_frequency_%28alphabetic%29.svg.png">
</p>

- Other possibilities include guessing the keys a and b.

# EXAMPLES

``` python
>>>from affine import Affine
>>>ciper = Affine()
>>>
>>>keys = ciper.getRandomKeys()
(42, 33)
>>> 
>>>text = 'Hello World'
>>>crypted = ciper.crypt(text, keys)
/d>>:xj:u>#
>>>
>>>ciper.decrypt(crypted, keys)
'Hello World'
```
